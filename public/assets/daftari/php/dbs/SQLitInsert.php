<?php
/**
 * file        : 20180331°0401
 * encoding    : UTF-8-without-BOM
 * ref         : http://www.sqlitetutorial.net/sqlite-php/insert/ [ref 20180330°0117]
 */

namespace Trekta\Databese;

//use Trekta\Daftari as TD;
//use Trekta\Daftari\Globals as Glb;

/**
 * This class demonstrates how to insert records into tables
 *
 * @id 20180331°0411
 * @author ncm
 */
class SQLitInsert {

   /**
    * PDO object
    *
    * @id 20180331°0421
    * @var \PDO
    */
   private $pdo;

   /**
    * This constructor initializes the  given object with a specified PDO
    *
    * @id 20180331°0422
    * @param \PDO $pdo
    */
   public function __construct($pdo) {
      $this->pdo = $pdo;
   }

   /**
    * This method inserts a new project record into the projects table
    *
    * @id 20180331°0423
    * @callers • Page 20180330°0311 sqlite7.html seq 20180330°0613 twice
    * @param string $projectName
    * @return String The id of the new project
    */
   public function insertProject($projectName)
   {
      $sql = 'INSERT INTO projects(project_name) VALUES(:project_name)';
      $stmt = $this->pdo->prepare($sql);
      $stmt->bindValue(':project_name', $projectName);
      $stmt->execute();

      return $this->pdo->lastInsertId();
   }

   /**
    * Insert a new task into the tasks table
    *
    * @id 20180331°0424
    * @callers • Page 20180330°0311 sqlite7.html seq 20180330°0613 seven times
    * @param String $taskName
    * @param String $startDate
    * @param String $completedDate
    * @param String $completed
    * @param String $projectId
    * @return String The ID of the inserted task
    */
   public function insertTask ( $taskName
                               , $startDate
                                , $completedDate
                                 , $completed
                                  , $projectId
                                   )
   {
      $sql = 'INSERT INTO tasks(task_name,start_date,completed_date,completed,project_id) '
            . 'VALUES(:task_name,:start_date,:completed_date,:completed,:project_id)'
             ;

      $stmt = $this->pdo->prepare($sql);
      $stmt->execute(
                [ ':task_name' => $taskName
                 , ':start_date' => $startDate
                  , ':completed_date' => $completedDate
                   , ':completed' => $completed
                    , ':project_id' => $projectId
                     , ])
                      ;

      return $this->pdo->lastInsertId();
   }
}

/* eof */
