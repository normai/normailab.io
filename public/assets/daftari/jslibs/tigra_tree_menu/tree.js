/*!
 *  ----------------------------------------------------
 *  Title: Tigra Tree
 *  Description: See the demo at url
 *  URL: http://www.softcomplex.com/products/tigra_tree_menu/
 *  Version: 1.1
 *  Date: 11-12-2002 (mm-dd-yyyy)
 *  Notes: This script is free. Visit official site for further details.
 *  ----------------------------------------------------
 *
 * file        : 20160416°1121 daftari/jslibs/tigra_tree_menu/tree.js
 * version     : v0.2.5 20210428°1151
 * license     : 'Feel free to modify'
 * copyright   : SoftComplex Inc.
 * authors     : SoftComplex Inc., modified by Norbert C. Maier
 * note        :
 */

/**
 * Versions
 *    • v0.2.5 20210428°1151 — Some streamlining
 *    • v0.2.1 20160501°1515 — Changes :
 *      ◦ Change encoding from ANSI to UTF-8-without-BOM [chg 20160501°1441]
 *      ◦ Reformat some sequences, notably breaking down long lines
 *      ◦ Fix icon path problem see issue 20160501°1411 [chg 20160501°1512/1513]
 *      ◦ Add doc-style function headers with IDs
 *      ◦ Follow some NetBeans warnings (Add semicolons, change some '==' to '===')
 *    • v0.2.0 20160416°1141 — Integration proof-of-concept
 *    • v0.1.1 20160416°1121 — Original file tagged as version 1.1
 * note : The 'rellay unchanged original' file header had the wrong
 *        URL http://www.softcomplex.com/products/tigra_menu_tree/
 */

//'use strict'; // chg 20201213°1011 : Line shutdown to avoid closure-compiler-v20190301.jar "WARNING - Suspicious code. Is there a missing '+' on the previous line?"


/**
 * Annouce namespace
 *
 * @id 20190106°0311`22
 * @c_o_n_s_t — Namespace
 */
var Trekta = Trekta || {};

/**
 * This function ...
 *
 * @id 20160416°1411
 * @callers • inline script sitemap.html • inline script treeview47.html
 * @param {Array} a_items — The tree items
 * @param {Object} a_template — The icon pathes
 * @return {undefined} —
 * @constructor —
 */
function tree (a_items, a_template) // original line
// //function tree (a_items, a_template, eTarget) // experimental parameter 20190404°0441 given up — not so easy
{
   'use strict';

   // Prefix icon pathes [seq 20190404°0623]
   var sPrfx = Trekta.Utils.s_DaftariBaseFolderAbs;
   for (var sProp in a_template) {
      if ( a_template.hasOwnProperty(sProp) ) {
         var s = sProp.substr(0,4);
         if ( s === 'icon' ) {
            a_template[sProp] = sPrfx + a_template[sProp];
         }
      }
   }
   // Now the icon pathes are fine. But they are set a second time, then broken.

   this.a_tpl      = a_template;
   this.a_config   = a_items;
   this.o_root     = this;
   this.a_index    = [];
   this.o_selected = null;
   this.n_depth    = -1;

   var o_icone = new Image();
   var o_iconl = new Image();
   o_icone.src = a_template['icon_e'];
   o_iconl.src = a_template['icon_l'];
   a_template['im_e'] = o_icone;
   a_template['im_l'] = o_iconl;
   for (var i = 0; i < 64; i++)
   {
      if (a_template['icon_' + i])
      {
         var o_icon = new Image();
         a_template['im_' + i] = o_icon;
         o_icon.src = a_template['icon_' + i];
      }
   }

   this.toggle = function (n_id) { var o_item = this.a_index[n_id]; o_item.open(o_item.b_opened); };
   this.select = function (n_id) { return this.a_index[n_id].select(); };
   this.mout   = function (n_id) { this.a_index[n_id].upstatus(true); };
   this.mover  = function (n_id) { this.a_index[n_id].upstatus(); };

   this.a_children22 = [];
   for (var i = 0; i < a_items.length; i++)
   {
      new tree_item(this, i);
   }

   this.n_id = trees.length;
   trees[this.n_id] = this;

   // Give up the idea in favour of letting be the inline-scripting [rem 20190404°0528]
   // // // See issue 20190404°0451 'tigratreeview injection after onload'
   // // if (typeof eTarget !== 'undefined') { // experimental condition [line 20190404°0442]
   // //
   // //    // try inject the tree but not via inline script [seq 20190404°0443]
   // //
   // //    // code adjusted for calls after onLoad [line 20190404°0444]
   // //    var sTree = '';
   // //    for (var i = 0; i < this.a_children22.length; i++)
   // //    {
   // //       sTree += this.a_children22[i].init(); // origianal line
   // //       this.a_children22[i].open();
   // //    }
   // //    eTarget.innerHTML = sTree;
   // //
   // // }
   // // else {

   // The original code
   for (var i = 0; i < this.a_children22.length; i++)
   {
      document.write(this.a_children22[i].init());                     // Original line
      this.a_children22[i].open();
   }

   // // }
}

/**
 * This function ...
 *
 * @id 20160416°1421
 * @param {Object} o_parent — ...
 * @param {number} n_order — ...
 * @return {undefined} —
 * @constructor —
 */
function tree_item (o_parent, n_order)
{
   'use strict';

   this.n_depth  = o_parent.n_depth + 1;
   this.a_config = o_parent.a_config[n_order + (this.n_depth ? 2 : 0)];
   if (! this.a_config)
   {
      return;
   }

   this.o_root    = o_parent.o_root;
   this.o_parent  = o_parent;
   this.n_order   = n_order;
   this.b_opened  = !this.n_depth;

   this.n_id = this.o_root.a_index.length;
   this.o_root.a_index[this.n_id] = this;
   o_parent.a_children22[n_order] = this;

   this.a_children22 = [];
   for (var i = 0; i < this.a_config.length - 2; i++)
   {
      new tree_item(this, i);
   }

   this.get_icon = item_get_icon;
   this.open     = item_open;
   this.select   = item_select;
   this.init     = item_init;
   this.upstatus = item_upstatus;
   this.is_last  = function () { return this.n_order === this.o_parent.a_children22.length - 1; };
}

/**
 * This function ...
 *
 * @id 20160416°1431
 * @param {boolean} b_close — ...
 * @return {undefined} —
 */
function item_open (b_close)
{
   'use strict';

   var o_idiv = get_element('i_div' + this.o_root.n_id + '_' + this.n_id);
   if (! o_idiv)
   {
      return;
   }

   if (! o_idiv.innerHTML)
   {
      var a_children33 = [];
      for (var i = 0; i < this.a_children22.length; i++)
      {
         a_children33[i] = this.a_children22[i].init();
      }
      // Compare seq 20190404°0623 'prefix icon pathes'
      o_idiv.innerHTML = a_children33.join('');
   }
   o_idiv.style.display = (b_close ? 'none' : 'block');

   this.b_opened = !b_close;
   var o_jicon = document.images['j_img' + this.o_root.n_id + '_' + this.n_id];
   var o_iicon = document.images['i_img' + this.o_root.n_id + '_' + this.n_id];
   if (o_jicon) { o_jicon.src = this.get_icon(true); }
   if (o_iicon) { o_iicon.src = this.get_icon(); }
   this.upstatus();
}

/**
 * This function ...
 *
 * @id 20160416°1441
 * @param {boolean} b_deselect — ...
 * @return {undefined} —
 */
function item_select (b_deselect)
{
   'use strict';

   if (! b_deselect)
   {
      var o_olditem = this.o_root.o_selected;
      this.o_root.o_selected = this;
      if (o_olditem) { o_olditem.select(true); }
   }
   var o_iicon = document.images['i_img' + this.o_root.n_id + '_' + this.n_id];
   if (o_iicon) { o_iicon.src = this.get_icon(); }
   get_element('i_txt' + this.o_root.n_id + '_' + this.n_id).style.fontWeight = b_deselect ? 'normal' : 'bold';

   this.upstatus();
   return Boolean(this.a_config[1]);
}

/**
 * This function ...
 *
 * @id 20160416°1451
 * @param {boolean} b_clear — ...
 * @return {undefined} —
 */
function item_upstatus (b_clear)
{
   'use strict';

   window.setTimeout( 'window.status="'
       + (b_clear ? '' : this.a_config[0]
        + (this.a_config[1] ? ' ('+ this.a_config[1] + ')' : ''))
         + '"', 10
          );
}

/**
 * This function ...
 *
 * @id 20160416°1511
 * @return {undefined} —
 */
function item_init ()
{
   'use strict';

   var a_offset = [];
   var o_current_item = this.o_parent;
   for (var i = this.n_depth; i > 1; i--)
   {
      // Compare seq 20190404°0623 'prefix icon pathes'
      a_offset[i] = '<img src="'
                   + this.o_root.a_tpl[o_current_item.is_last() ? 'icon_e' : 'icon_l']
                    + '" border="0" align="absbottom">'
                     ;
      o_current_item = o_current_item.o_parent;
   }

   // Debug issue 20160501°1411 [seq 20160501°1431]
   var xRet = '<table cellpadding="0" cellspacing="0" border="0"><tr><td nowrap>'
            + ( this.n_depth ? a_offset.join('')
                 + ( this.a_children22.length
                      ? '<a href="javascript: trees[' + this.o_root.n_id + '].toggle('
                      + this.n_id + ')" onmouseover="trees[' + this.o_root.n_id + '].mover('
                      + this.n_id + ')" onmouseout="trees[' + this.o_root.n_id + '].mout('
                      + this.n_id + ')"><img src="' + this.get_icon(true)
                      + '" border="0" align="absbottom" name="j_img' + this.o_root.n_id + '_'
                      + this.n_id + '"></a>'
                      : '<img src="' + this.get_icon(true) + '" border="0" align="absbottom">'
                   ) : ''
              )
            + '<a href="' + this.a_config[1] + '" target="' + this.o_root.a_tpl['target']
         + '" onclick="return trees[' + this.o_root.n_id + '].select(' + this.n_id
         + ')" ondblclick="trees[' + this.o_root.n_id + '].toggle(' + this.n_id
         + ')" onmouseover="trees[' + this.o_root.n_id + '].mover(' + this.n_id
         + ')" onmouseout="trees[' + this.o_root.n_id + '].mout(' + this.n_id
         + ')" class="t' + this.o_root.n_id + 'i" id="i_txt' + this.o_root.n_id
         + '_' + this.n_id + '"><img src="' + this.get_icon()
         + '" border="0" align="absbottom" name="i_img' + this.o_root.n_id
         + '_' + this.n_id + '" class="t' + this.o_root.n_id + 'im">'
         + this.a_config[0] + '</a></td></tr></table>'
         + ( this.a_children22.length ? '<div id="i_div' + this.o_root.n_id
             + '_' + this.n_id + '" style="display:none"></div>' : ''
           )
         ;
   return xRet;
}

/**
 * This function ...
 *
 * @id 20160416°1521
 * @param {boolean} b_junction — ...
 * @return {undefined} —
 */
function item_get_icon (b_junction)
{
   'use strict';

   // Debug issue 20160501°1411 [seq 20160501°1421]
   var xNdx = 'icon_' + ((this.n_depth ? 0 : 32) + (this.a_children22.length ? 16 : 0)
             + (this.a_children22.length && this.b_opened ? 8 : 0)
             + (! b_junction && this.o_root.o_selected === this ? 4 : 0)
             + (b_junction ? 2 : 0) + (b_junction && this.is_last() ? 1 : 0)
             );
   var xRet = this.o_root.a_tpl[xNdx];
   return xRet;
}

/**
 * This variable ...
 *
 * @id 20160416°1531
 * @type {Array} —
 */
var trees = [];

/**
 * This sequence defines a custom function to get an element by ID, different
 *  depending on whether the browsers has an 'document .all' array or not
 *
 * @id 20160416°1541
 * @param {string} s_id — The ID of the wanted element
 * @returns {undefined} —
 */
var get_element = document.all
                 ? function (s_id) { return document.all[s_id]; }
                  : function (s_id) { return document.getElementById(s_id); }
                   ;

/* eof tree.js */

// Cut-n-paste from tree_tpl.js to tree.js [chg 20190404°0351]
﻿/* - - - ✂ - - - - - - - - - - - - - - - - - - - - - - - - - - */
//-----------------------------------------------------
/*
   Feel free to use your custom icons for the tree. Make sure they are all the same
   size. User icons collections are welcome, we'll publish them giving all regards.
*/
//-----------------------------------------------------
/**
 * This file provides the icon URLs for the tree view
 *
 * file : 20160416°1131 daftari/jslibs/tigra_tree_menu/tree_tpl.js
 * license : 'Free'
 * copyright : SoftComplex Inc.
 * authors : SoftComplex Inc., with modifications by ncm
 * note : Encoding changed to UTF-8-with-BOM [chg 20160501°1441]
 * note : Original file header see above.
 */

/**
 * This variable provides the icon URLs for the tree view
 *
 * @id  20160416°1133
 * @ {Object} —
 */
var DAF_TREE_TPL = {                                                   // [chg 20210416°1612`01]

   // Possible values are: _blank, _parent, _search, _self, _top and frameset
   'target'  : '_self',

   'icon_e'  : '/jslibs/tigra_tree_menu/icons1/empty.gif',             // empty image
   'icon_l'  : '/jslibs/tigra_tree_menu/icons1/line.gif',              // vertical line

   'icon_32' : '/jslibs/tigra_tree_menu/icons1/base.gif',              // root leaf icon normal
   'icon_36' : '/jslibs/tigra_tree_menu/icons1/base.gif',              // root leaf icon selected

   'icon_48' : '/jslibs/tigra_tree_menu/icons1/base.gif',              // root icon normal
   'icon_52' : '/jslibs/tigra_tree_menu/icons1/base.gif',              // root icon selected
   'icon_56' : '/jslibs/tigra_tree_menu/icons1/base.gif',       // top // root icon opened
   'icon_60' : '/jslibs/tigra_tree_menu/icons1/base.gif',              // root icon selected

   'icon_16' : '/jslibs/tigra_tree_menu/icons1/folder.gif',            // node icon normal
   'icon_20' : '/jslibs/tigra_tree_menu/icons1/folderopen.gif',        // node icon selected
   'icon_24' : '/jslibs/tigra_tree_menu/icons1/folderopen.gif',        // node icon opened
   'icon_28' : '/jslibs/tigra_tree_menu/icons1/folderopen.gif',        // node icon selected opened

   'icon_0'  : '/jslibs/tigra_tree_menu/icons1/page.gif',              // leaf icon normal
   'icon_4'  : '/jslibs/tigra_tree_menu/icons1/page.gif',              // leaf icon selected

   'icon_2'  : '/jslibs/tigra_tree_menu/icons1/joinbottom.gif',        // junction for leaf
   'icon_3'  : '/jslibs/tigra_tree_menu/icons1/join.gif',              // junction for last leaf
   'icon_18' : '/jslibs/tigra_tree_menu/icons1/plusbottom.gif',        // junction for closed node
   'icon_19' : '/jslibs/tigra_tree_menu/icons1/plus.gif',              // junction for last closed node
   'icon_26' : '/jslibs/tigra_tree_menu/icons1/minusbottom.gif',       // junction for opened node
   'icon_27' : '/jslibs/tigra_tree_menu/icons1/minus.gif'              // junction for last opended node
};
﻿/* - - - ✂ - - - - - - - - - - - - - - - - - - - - - - - - - - */
