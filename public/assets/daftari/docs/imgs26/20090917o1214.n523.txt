﻿
   img 20090917°1214
   objecttype  : img.digitalphoto
   title       : Speisepilz
   summary     :
   location    : Weißenseifen
   event       :
   policy      : public and redistributable under conditions
   license     : CC BY-SA 3.0 http://creativecommons.org/licenses/by-sa/3.0 [ref 20110922°2121]
   copyright   : © Norbert C. Maier
   author      : Norbert C. Maier
   owner       : Norbert C. Maier
   albums      : nature
   keywords    : nature
   objectsize  :
   imagesize   :
   camera      : Nokia-E90
   processing  : v1: GIMP square; brightness +0 contrast +20;
   quality     :
   note        :
   ⬞Ω
