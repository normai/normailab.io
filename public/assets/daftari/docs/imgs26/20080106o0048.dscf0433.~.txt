﻿
   img 20080106°0048
   objecttype  : img.digitalphoto
   title       : Omega
   event       : Party
   policy      : public and redistributable under conditions
   license     : CC BY-SA 3.0 http://creativecommons.org/licenses/by-sa/3.0 [ref 20110922°2121]
   copyright   :
   author      : ncm
   keywords    :
   albums      : light
   processing  :
   quality     :
   note        :
   ⬞Ω
